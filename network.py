import random


class NeuralNetwork:

    class Layer:
        def __init__(self, count=2, last_count=2, fill_with_number=1):
            self.weights = [fill_with_number for _ in range(count * last_count)]
            self.last = self.weights.copy()
            self.count = count

        def mutate(self, mutations=1.0, mutations_chance=30, direction=0.0):
            self.last = self.weights.copy()
            for i, n in enumerate(self.weights[:]):
                if random.randint(1, 100) <= mutations_chance:
                    if direction:
                        self.weights[i] += random.uniform(0, mutations) * direction
                    else:
                        self.weights[i] += random.uniform(-mutations, mutations)

        def reload(self):
            self.weights = self.last.copy()

        def calculate(self, in_put=(0, 0), activation_func=float):
            output = [0 for _ in range(self.count)]
            end_pos = 0
            for i in in_put:
                for j, _ in enumerate(output[:]):
                    output[j] += i * self.weights[end_pos]
                    end_pos += 1

            output = tuple(map(activation_func, output))
            return output

        def copy(self):
            layer = NeuralNetwork.Layer()
            layer.weights = self.weights.copy()
            layer.last = self.last
            layer.count = self.count
            return layer

    def __init__(self, layers=(2, 3, 2), activation_func=float, make_last_static=False):
        self.score = 0
        self.last_score = 0
        self.activation_func = activation_func
        self.layers = []
        self.make_last_static = make_last_static
        for i, n in enumerate(layers):
            if i > 0:
                self.\
                    layers.append(self.Layer(n - (1 if make_last_static and i < len(layers) - 1 else 0), layers[i - 1]))
            else:
                self.input = n

    def run_variants(self,
                     population=10,
                     mutations=1,
                     mutation_chance=30,
                     run_funk=None,
                     change_to_best=True,
                     run_with_self=False):
        high_score = ''
        # print(self, 'start', self.layers)
        for _ in range(population):
            network = NeuralNetwork(activation_func=self.activation_func,
                                    layers=())
            network.layers = self.layers.copy()
            for i, n in enumerate(network.layers):
                network.layers[i] = n.copy()
            network.mutate(mutations=mutations,
                           mutation_chance=mutation_chance)
            score = run_funk(network)
            if high_score == '' or score > high_score[0]:
                # print(high_score, score)
                high_score = (score, network)
        if run_with_self:
            score = run_funk(self)
            if high_score == '' or score > high_score[0]:
                # print('cat', high_score, score)
                high_score = (score, self)
        # print(high_score, 'final', high_score[1].layers)
        if change_to_best:
            self.layers = high_score[1].layers
        return high_score

    def check_loss(self, wanted_result, given_data, neural_network=None):
        if neural_network:
            result = neural_network.calculate(given_data)
        else:
            result = self.calculate(given_data)
        loss = 0
        for i, n in enumerate(result):
            loss += abs(n - wanted_result[i])
        return loss

    def learn(self, wanted_result, given_data, mutations_range=0.01):
        loss = self.check_loss(wanted_result, given_data)
        network_copy = self.copy()
        if loss:
            for i, n in enumerate(network_copy.layers):
                for j, _ in enumerate(n.weights):
                    n.weights[j] += mutations_range
                    loss2 = network_copy.check_loss(wanted_result, given_data)
                    if loss2 < loss:
                        loss = loss2
                    else:
                        n.weights[j] -= mutations_range * 2
                        loss2 = network_copy.check_loss(wanted_result, given_data)
                        if loss2 < loss:
                            loss = loss2
                        else:
                            n.weights[j] = self.layers[i].weights[j]
        self.layers = network_copy.layers
        return loss

    def copy(self):
        network = NeuralNetwork(activation_func=self.activation_func, make_last_static=self.make_last_static, layers=())
        network.layers = []
        for i in self.layers:
            network.layers.append(i.copy())
        return network

    def mutate(self,
               mutations=1.0,
               mutation_chance=30,
               direction=0.0):
        for i in self.layers:
            i.mutate(mutations=mutations,
                     mutations_chance=mutation_chance,
                     direction=direction)

    def add_score(self, count=1):
        self.score += count

    def calculate(self, in_put=(0, 0)):
        last_input = in_put
        for i, n in enumerate(self.layers):
            last_input = n.calculate(last_input, self.activation_func)
            if 0 < i < len(self.layers) - 1 and self.make_last_static:
                last_input = (*last_input, 1)

        return last_input

